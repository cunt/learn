#!/usr/bin/env python3
# compound interest

P = float(input("Enter the principal     "))
r = float(input("Enter the annual interest rate as decimal     "))
n = float(input("Enter the number of times interest is compounded per year     "))
t = float(input("Enter the number of years   "))
final_amount = P * ((1+r/n) ** (n*t))

print("final amount is ", final_amount )
