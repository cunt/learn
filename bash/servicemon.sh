#!/bin/bash

# script to monitor the availability of a service

##### EXIT CODES ##################
# 1 : no argument provided
# 2 : Service is not running
###################################


# make sure that service name is provided as an argument

if [ -z $1 ];
then
    echo you need to provide the name of service as an argument
    exit 1
else
    SERVICE=$1
fi





# run without stopping to monitor

## verify that $SERVICE is running
if ps aux | grep $SERVICE | grep -v grep | grep -v servicemon
then
    echo $SERVICE is running now!!
else
    echo $SERVICE could not be found as a process
    echo Make sure $SERVICE is running and try again.
    echo "the command \`ps aux | grep $SERVICE\` should show the service"
    exit 2
fi

## monitor the $SERVICE
while ps aux | grep $SERVICE | grep -v grep | grep -v servicemon
do
    sleep 10
done





# actions if service is failing
### assume that service process name could be started with service command.
### to be improved #WIP

service $SERVICE start #making using of backward compatibility of systemd
logger servicemon: $SERVICE started at $(date +%Y-%m-%d\ %H:%M)
mail -s "servicemon: $SERVICE started at $(date +%Y-%m-%d\ %H:%M)"  root < .





